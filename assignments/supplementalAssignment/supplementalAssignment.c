#include "pico/stdlib.h"
#include  "hardware/gpio.h"
// Must declare the main assembly entry point before use.
void main_asm();
/**
* @brief Supplemental Assignment...
*
* @return int Returns exit-status zero on completion.
*/

//Function to init gpio to be called in main_asm
void asm_gpio_init(int pin){
    gpio_init(pin);
}
//Function to turn gpio pins off
void asm_gpio_put(int pin,bool val){
    gpio_put(pin,val);
}
//Function to set pin to output
void asm_gpio_out(int pin, bool out){
    gpio_set_dir(pin, out);
}
//Function to sleep for 
void asm_sleep_ms(int ms){
    sleep_ms(ms);
}

int main() {

 // Jump into the main assembly code subroutine that implements the project.
 main_asm();
 // Returning zero indicates everything went okay.
 return 0;
}